/*
 * Kanaparthi Tarun Santosh
 */
package com.tkanapar.sqldb;

import java.util.ArrayList;
import java.util.Locale;

import com.google.android.gms.maps.model.LatLng;
import com.tkanapar.util.TagSav;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.location.Address;

public class TagSavDao {
	private SQLiteDatabase db;
	private String[] AllColumns = new String[] {
			TagSavTable.TAGSAV_ADDRESSLINE0, TagSavTable.TAGSAV_ADDRESSLINE1,
			TagSavTable.TAGSAV_ADDRESSLINE2, TagSavTable.TAGSAV_LATITUDE,
			TagSavTable.TAGSAV_LONGITUDE, TagSavTable.TAGSAV_DESCRIPTION,
			TagSavTable.TAGSAV_FORMATTEDDATE,TagSavTable.TAGSAV_ID };

	public TagSavDao(SQLiteDatabase db) {
		this.db = db;
	}

	public long save(TagSav tagsav) {
		ContentValues values = new ContentValues();
		Address address;
		if ((address = tagsav.getAddress()) != null) {
			for (int i = 0; i < 3; i++) {
				if (address.getAddressLine(i) == null) {
					address.setAddressLine(i, "Not Available !!");
					;
				}
			}
		}

		values.put(TagSavTable.TAGSAV_ADDRESSLINE0, address.getAddressLine(0));
		values.put(TagSavTable.TAGSAV_ADDRESSLINE1, address.getAddressLine(1));
		values.put(TagSavTable.TAGSAV_ADDRESSLINE2, address.getAddressLine(2));
		values.put(TagSavTable.TAGSAV_LATITUDE, address.getLatitude());
		values.put(TagSavTable.TAGSAV_LONGITUDE, address.getLongitude());
		values.put(TagSavTable.TAGSAV_DESCRIPTION, tagsav.getDescription());
		values.put(TagSavTable.TAGSAV_FORMATTEDDATE, tagsav.getFormattedDate());
		return db.insert(TagSavTable.TABLE_NAME, null, values);
	}

	public boolean delete() {
		return db.delete(TagSavTable.TABLE_NAME, null, null) > 0;
	}
	
	public boolean delete(Integer _ID){
		return db.delete(TagSavTable.TABLE_NAME, TagSavTable.TAGSAV_ID +" = "+_ID, null)>0;
	}
	public ArrayList<TagSav> getAll() {
		ArrayList<TagSav> list = new ArrayList<TagSav>();

		Cursor c = db.query(TagSavTable.TABLE_NAME, AllColumns, null, null,
				null, null, TagSavTable.TAGSAV_ID+" DESC");
		if (c != null && c.moveToFirst()) {
			do {
				TagSav tagsav = this.buildNoteFromCursor(c);
				if (tagsav != null) {
					list.add(tagsav);
				}
			} while (c.moveToNext());

			if (!c.isClosed()) {
				c.close();
			}
		}
		return list;
	}
	public ArrayList<TagSav> getAllAsc() {
		ArrayList<TagSav> list = new ArrayList<TagSav>();

		Cursor c = db.query(TagSavTable.TABLE_NAME, AllColumns, null, null,
				null, null, TagSavTable.TAGSAV_ID+" ASC");
		if (c != null && c.moveToFirst()) {
			do {
				TagSav tagsav = this.buildNoteFromCursor(c);
				if (tagsav != null) {
					list.add(tagsav);
				}
			} while (c.moveToNext());

			if (!c.isClosed()) {
				c.close();
			}
		}
		return list;
	}
	public void updateTagSav(TagSav tagsav){
		ContentValues values = new ContentValues();
		Address address;
		if ((address = tagsav.getAddress()) != null) {
			for (int i = 0; i < 3; i++) {
				if (address.getAddressLine(i) == null) {
					address.setAddressLine(i, "Not Available !!");
					;
				}
			}
		}

		values.put(TagSavTable.TAGSAV_ADDRESSLINE0, address.getAddressLine(0));
		values.put(TagSavTable.TAGSAV_ADDRESSLINE1, address.getAddressLine(1));
		values.put(TagSavTable.TAGSAV_ADDRESSLINE2, address.getAddressLine(2));
		values.put(TagSavTable.TAGSAV_LATITUDE, address.getLatitude());
		values.put(TagSavTable.TAGSAV_LONGITUDE, address.getLongitude());
		values.put(TagSavTable.TAGSAV_DESCRIPTION, tagsav.getDescription());
		values.put(TagSavTable.TAGSAV_FORMATTEDDATE, tagsav.getFormattedDate());
		db.update(TagSavTable.TABLE_NAME, values, TagSavTable.TAGSAV_ID+" = "+ tagsav.get_ID(), null);
	}

	public ArrayList<TagSav> getTagFromLatLng(LatLng latlng) {
		ArrayList<TagSav> list = new ArrayList<TagSav>();
		
		Cursor c = db.query(TagSavTable.TABLE_NAME, AllColumns,
				TagSavTable.TAGSAV_LATITUDE + " =" + latlng.latitude + " and "
						+ TagSavTable.TAGSAV_LONGITUDE + " = "
						+ latlng.longitude, null, null, null, TagSavTable.TAGSAV_ID+" DESC");
		
		if (c != null && c.moveToFirst()) {
			do {
				TagSav tagsav = this.buildNoteFromCursor(c);
				if (tagsav != null) {
					list.add(tagsav);
				}
			} while (c.moveToNext());

			if (!c.isClosed()) {
				c.close();
			}
		}
		return list;
	}

	private TagSav buildNoteFromCursor(Cursor c) {
		TagSav tagsav = null;
		if (c != null) {
			tagsav = new TagSav();
			Locale locale = new Locale("en", "US");
			Address address = new Address(locale);
			address.setAddressLine(0, c.getString(0));
			address.setAddressLine(1, c.getString(1));
			address.setAddressLine(2, c.getString(2));
			address.setLatitude(c.getDouble(3));
			address.setLongitude(c.getDouble(4));
			tagsav.setAddress(address);
			tagsav.setDescription(c.getString(5));
			tagsav.setFormattedDate(c.getString(6));
			tagsav.set_ID(c.getInt(7));

		}
		return tagsav;
	}
}
