/*
 * Kanaparthi Tarun Santosh
 */
package com.tkanapar.sqldb;

import java.util.ArrayList;

import com.google.android.gms.maps.model.LatLng;
import com.tkanapar.util.TagSav;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;



public class DatabaseManager {
	Context mContext;
	DatabaseHelper dbOpenHelper;
	SQLiteDatabase db;
	TagSavDao tagsavDao;

	public DatabaseManager(Context mContext) {
		this.mContext = mContext;
		dbOpenHelper = new DatabaseHelper(mContext);
		db = dbOpenHelper.getWritableDatabase();
		tagsavDao = new TagSavDao(db);
	}

	public void close() {
		db.close();
	}

	public long saveTagSav(TagSav tagsav) {
		return tagsavDao.save(tagsav);
	}
	
	public void updateTagSav(TagSav tagsav){
		tagsavDao.updateTagSav(tagsav);
	}
	
	public boolean deleteTagSav() {
		return tagsavDao.delete();
	}
	public boolean deleteTagSav(Integer _ID) {
		return tagsavDao.delete(_ID);
	}
	public ArrayList<TagSav> getAllTagSavs() {
		return tagsavDao.getAll();
	}
	public ArrayList<TagSav> getAllTagSavsAsc() {
		return tagsavDao.getAllAsc();
	}
	public ArrayList<TagSav> getTagFromLatLng(LatLng latlng){
		return tagsavDao.getTagFromLatLng(latlng);
	}
}