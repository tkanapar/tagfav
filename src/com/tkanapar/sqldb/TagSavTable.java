/*
 * Kanaparthi Tarun Santosh
 */
package com.tkanapar.sqldb;

import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

public class TagSavTable {

	static final String TABLE_NAME = "TagSav";
	static final String TAGSAV_ID = "_ID";
	static final String TAGSAV_ADDRESSLINE0 = "addressLine0";
	static final String TAGSAV_ADDRESSLINE1 = "addressLine1";
	static final String TAGSAV_ADDRESSLINE2 = "addressLine2";
	static final String TAGSAV_LATITUDE = "latitude";
	static final String TAGSAV_LONGITUDE = "longitude";
	static final String TAGSAV_DESCRIPTION = "description";
	static final String TAGSAV_FORMATTEDDATE = "formatteddate";

	static public void onCreate(SQLiteDatabase db) {
		StringBuilder sb = new StringBuilder();
		sb.append("CREATE TABLE " + TagSavTable.TABLE_NAME + " (");
		sb.append(TagSavTable.TAGSAV_ID + " integer primary key, ");
		sb.append(TagSavTable.TAGSAV_ADDRESSLINE0 + " text, ");
		sb.append(TagSavTable.TAGSAV_ADDRESSLINE1 + " text, ");
		sb.append(TagSavTable.TAGSAV_ADDRESSLINE2 + " text, ");
		sb.append(TagSavTable.TAGSAV_LATITUDE + " REAL not null, ");
		sb.append(TagSavTable.TAGSAV_LONGITUDE + " REAL not null, ");
		sb.append(TagSavTable.TAGSAV_DESCRIPTION + " text not null, ");
		sb.append(TagSavTable.TAGSAV_FORMATTEDDATE + " text not null ");

		sb.append(");");
		try {
			db.execSQL(sb.toString());
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	static public void onUpgrade(SQLiteDatabase db, int oldVersion,
			int newVersion) {
		db.execSQL("DROP TABLE IF EXISTS " + TagSavTable.TABLE_NAME);
		TagSavTable.onCreate(db);

	}

}
