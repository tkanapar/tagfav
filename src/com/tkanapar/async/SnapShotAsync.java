/*
 * Kanaparthi Tarun Santosh
 */
package com.tkanapar.async;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;

import com.google.android.gms.maps.GoogleMap.SnapshotReadyCallback;
import com.tkanapar.fragment.TagFragment;
import com.tkanapar.tagfav.MainActivity;

import android.graphics.Bitmap;
import android.os.AsyncTask;

public class SnapShotAsync extends AsyncTask<String, Void, Void> {
	TagFragment tF;
	public SnapShotAsync(TagFragment tF){
		this.tF = tF;
	}
	@Override
	protected Void doInBackground(final String... params) {

		SnapshotReadyCallback callback = new SnapshotReadyCallback() {
		String filename = params[0];
			@Override
			public void onSnapshotReady(Bitmap snapMapShot) {
				tF.snapMap = snapMapShot;
				
				try {
				    File file = new File(MainActivity.context.getExternalFilesDir(null), filename);
					FileOutputStream out = new FileOutputStream(file);
					tF.snapMap.compress(Bitmap.CompressFormat.JPEG, 30, out);
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				}
			}
		};
		tF.gMap.snapshot(callback);
		return null;
	}

}
