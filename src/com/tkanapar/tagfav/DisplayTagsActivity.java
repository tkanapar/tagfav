/*
 * Kanaparthi Tarun Santosh
 */
package com.tkanapar.tagfav;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.ArrayList;

import com.tkanapar.adapter.TagSavDisplayAdapter;
import com.tkanapar.sqldb.DatabaseManager;
import com.tkanapar.util.StringUtil;
import com.tkanapar.util.TagSav;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.NavUtils;
import android.support.v7.app.ActionBarActivity;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ContextMenu.ContextMenuInfo;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.Toast;

public class DisplayTagsActivity extends ActionBarActivity implements
		OnItemClickListener {
	TagSavDisplayAdapter tagSavDisplayAdapter;
	ListView listView;
	DatabaseManager dm;
	ArrayList<TagSav> tagsavs = null;
	static final int REQUEST_IMAGE_CAPTURE = 1;
	static final int REQUEST_CHANGE_TEXT = 199;
	Bitmap resultBitmap;
	String filename;
	File file;
	static int oposition;
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.fragment_fav);
		listView = (ListView) findViewById(R.id.lvFav);
		setupFavDisplay();
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			NavUtils.navigateUpFromSameTask(this);
			return true;
		}
		return false;
	}
	

	public void setupFavDisplay() {

		tagsavs = getIntent().getExtras().getParcelableArrayList(
				StringUtil.TAGSAVS_KEY);

		if (tagsavs == null) {
			Toast.makeText(this, "No tags saved !!", Toast.LENGTH_LONG).show();
			return;
		}
		tagSavDisplayAdapter = new TagSavDisplayAdapter(this, tagsavs);
		if (tagSavDisplayAdapter == null) {
			Toast.makeText(this, "Problem with adapter !!", Toast.LENGTH_LONG)
					.show();
			return;
		}
		listView.setAdapter(tagSavDisplayAdapter);
		listView.setOnItemClickListener(this);
		registerForContextMenu(listView);

	}

	@Override
	public boolean onContextItemSelected(MenuItem item) {
		AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item
				.getMenuInfo();
		int menuItemIndex = item.getItemId();
		String[] menuItems = getResources().getStringArray(R.array.menu);
		String menuItemName = menuItems[menuItemIndex];
		TagSav tagsav = tagsavs.get(info.position);

		Integer _ID = tagsav.get_ID();
		final String formattedDate = tagsav.getFormattedDate();
		filename = StringUtil.SNAPFILE + formattedDate + ".jpg";
		String filePath = MainActivity.context.getExternalFilesDir(null) + "/"
				+ filename;
		file = new File(filePath);
		if (menuItemName.equals("Delete")) {
			tagSavDisplayAdapter.remove(tagsav);
			file.delete();
			dm = new DatabaseManager(MainActivity.context);
			dm.deleteTagSav(_ID);
			dm.close();
			Toast.makeText(MainActivity.context, "Deleted " + _ID,
					Toast.LENGTH_SHORT).show();
		} else if (menuItemName.equals("Change Picture")) {
			tagSavDisplayAdapter.remove(tagsav);
			dispatchTakePictureIntent();
			tagSavDisplayAdapter.insert(tagsav, info.position);

		}

		return true;
	}

	private void dispatchTakePictureIntent() {
		Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		if (takePictureIntent.resolveActivity(MainActivity.context
				.getPackageManager()) != null) {
			startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);

		}

	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == REQUEST_IMAGE_CAPTURE
				&& resultCode == MainActivity.RESULT_OK) {
			Bundle extras = data.getExtras();
			Bitmap resultBitmap = (Bitmap) extras.get("data");
			if (resultBitmap != null) {
				file.delete();
				file = new File(MainActivity.context.getExternalFilesDir(null),
						filename);
				try {
					FileOutputStream out = new FileOutputStream(file);
					resultBitmap.compress(Bitmap.CompressFormat.JPEG, 100, out);
					setupFavDisplay();

				} catch (FileNotFoundException e) {
					Toast.makeText(MainActivity.context, "File not found !!",
							Toast.LENGTH_SHORT).show();
				}

			}
			return;
		}
		if (requestCode == REQUEST_CHANGE_TEXT) {
			TagSav tagsav=(TagSav)data.getExtras().getParcelable("tagsavup");
			tagSavDisplayAdapter.remove(tagsav);
			tagSavDisplayAdapter.insert(tagsav, oposition);
			
		}
	}

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v,
			ContextMenuInfo menuInfo) {
		if (v.getId() == R.id.lvFav) {
			AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuInfo;
			menu.setHeaderTitle(tagsavs.get(info.position).getFormattedDate()
					.replace("_", " "));
			String[] menuItems = getResources().getStringArray(R.array.menu);
			for (int i = 0; i < menuItems.length; i++) {
				menu.add(Menu.NONE, i, i, menuItems[i]);
			}
		}
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		oposition = position;
		TagSav tagsav = tagsavs.get(position);
		Intent intent = new Intent(DisplayTagsActivity.this,
				DisplaySavedActivity.class);
		intent.putExtra(StringUtil.DTAGSAV_KEY, tagsav);
		startActivityForResult(intent, REQUEST_CHANGE_TEXT);
	}

}
