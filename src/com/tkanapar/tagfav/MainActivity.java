/*
 * Kanaparthi Tarun Santosh
 */
package com.tkanapar.tagfav;

import com.tkanapar.fragment.FavFragment;
import com.tkanapar.fragment.TagFragment;
import com.tkanapar.fragment.ViewFragment;

import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBar.Tab;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.app.Activity;
import android.content.Context;
import android.location.Geocoder;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Vibrator;
import android.support.v4.view.ViewPager;
import android.view.Menu;
import android.view.MenuItem;

public class MainActivity extends ActionBarActivity {

	/**
	 * The {@link ViewPager} that will host the section contents.
	 */
	public static Geocoder geoCoder;
	public static Vibrator v;
	public static android.app.FragmentManager fragmentManager;
	public static LocationManager locationManager;
	public static Context context;
	Fragment mFragment;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		final ActionBar actionBar = getSupportActionBar();
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
		actionBar.setDisplayShowTitleEnabled(false);

		context = getApplicationContext();
		fragmentManager = getFragmentManager();
		getBaseContext();
		locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
		v = (Vibrator) getBaseContext().getSystemService(
				Context.VIBRATOR_SERVICE);
		geoCoder = new Geocoder(getBaseContext());

		Tab tab = actionBar
				.newTab()
				.setText(R.string.Tag_tab)
				.setTabListener(
						new TabListener<TagFragment>(this, "Tag",
								TagFragment.class));
		actionBar.addTab(tab);

		tab = actionBar
				.newTab()
				.setText(R.string.Fav_tab)
				.setTabListener(
						new TabListener<FavFragment>(this, "Fav",
								FavFragment.class));
		actionBar.addTab(tab);

		tab = actionBar
				.newTab()
				.setText(R.string.View_tab)
				.setTabListener(
						new TabListener<ViewFragment>(this, "View",
								ViewFragment.class));
		actionBar.addTab(tab);

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		int id = item.getItemId();
		switch (id) {
		case R.id.action_settings:
			return true;
		
		}
		return super.onOptionsItemSelected(item);

	}

	public static class TabListener<T extends Fragment> implements
			ActionBar.TabListener {
		private Fragment mFragment;
		private final Activity mActivity;
		private final String mTag;
		private final Class<T> mClass;

		public TabListener(Activity activity, String tag, Class<T> clz) {
			mActivity = activity;
			mTag = tag;
			mClass = clz;
		}

		public void onTabSelected(Tab tab, FragmentTransaction ft) {
			if (mFragment == null) {
				mFragment = Fragment.instantiate(mActivity, mClass.getName());
				ft.add(android.R.id.content, mFragment, mTag);
			} else {
				ft.attach(mFragment);
			}
		}

		public void onTabUnselected(Tab tab, FragmentTransaction ft) {

			if (mFragment != null) {
				ft.detach(mFragment);
			}
		}

		public void onTabReselected(Tab tab, FragmentTransaction ft) {
		}
	}

}
