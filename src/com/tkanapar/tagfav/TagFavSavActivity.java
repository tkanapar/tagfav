/*
 * Kanaparthi Tarun Santosh
 */
package com.tkanapar.tagfav;

import com.tkanapar.sqldb.DatabaseManager;
import com.tkanapar.util.StringUtil;
import com.tkanapar.util.TagSav;
import com.tkanapar.util.TagSavUtil;

import android.content.Intent;
import android.graphics.Bitmap;
import android.location.Address;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v7.app.ActionBarActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class TagFavSavActivity extends ActionBarActivity {
	Address address;
	ImageView imSnapMap;
	TextView tvAddress;
	TextView tvDate;
	EditText etDescription;
	TextView tvDescription;
	TagSav tagsav;
	Button bSave;
	Button bBack;
	DatabaseManager dm;
	String filename;
	static final int REQUEST_IMAGE_CAPTURE = 1;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	
		setContentView(R.layout.activity_tagfavsav);
		imSnapMap = (ImageView) findViewById(R.id.imSnapMap);
		tvAddress = (TextView) findViewById(R.id.tvAddress);
		etDescription = (EditText) findViewById(R.id.etDescription);
		tvDescription = (TextView) findViewById(R.id.tvDescription);
		tvDate = (TextView) findViewById(R.id.tvDate);
		bSave = (Button) findViewById(R.id.bSave);
		bBack = (Button) findViewById(R.id.bBack);
		bSave.setVisibility(View.VISIBLE);
		bBack.setVisibility(View.INVISIBLE);
		etDescription.setVisibility(View.VISIBLE);
		tvDescription.setVisibility(View.INVISIBLE);
		
		setUpLayout();
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			NavUtils.navigateUpFromSameTask(this);
			return true;
		}
		return false;
	}

	public void setUpLayout() {
		tagsav = (TagSav) getIntent().getExtras().getParcelable(
				StringUtil.TAGSAV_KEY);
		address = tagsav.getAddress();
		String formattedDate = tagsav.getFormattedDate();

		filename = StringUtil.SNAPFILE + formattedDate + ".jpg";
		Bitmap bitmap = TagSavUtil.decodeSampledBitmapFromResource(
				getExternalFilesDir(null) + "/" + filename, 480, 220);

		if (bitmap != null) {
			imSnapMap.setImageBitmap(bitmap);
		}
		tvDate.setText(formattedDate.replace("_", " "));
		if (address != null) {
			Double latitude = TagSavUtil
					.roundFiveDecimal(address.getLatitude());
			Double longitude = TagSavUtil.roundFiveDecimal(address
					.getLongitude());

			StringBuilder displayAddress = new StringBuilder();
			displayAddress.append(address.getAddressLine(0));
			displayAddress.append("\n");

			displayAddress.append(address.getAddressLine(1) + " "
					+ address.getAddressLine(2));
			displayAddress.append("\n");
			displayAddress.append("Latitude=" + latitude);
			displayAddress.append(" Longitude=" + longitude);
			tvAddress.setText(displayAddress.toString());
		}

	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
			Bundle extras = data.getExtras();
			Bitmap imageBitmap = (Bitmap) extras.get("data");
			imSnapMap.setImageBitmap(imageBitmap);
		}
	}

	public void setPrecision() {
		Double latitude = TagSavUtil.roundFiveDecimal(tagsav.getAddress()
				.getLatitude());
		Double longitude = TagSavUtil.roundFiveDecimal(tagsav.getAddress()
				.getLongitude());
		tagsav.getAddress().setLatitude(latitude);
		tagsav.getAddress().setLongitude(longitude);
	}

	public void Back(View view) {
		finish();
	}

	public void saveDisplay(View view) {
		bSave.setVisibility(View.INVISIBLE);
		bBack.setVisibility(View.VISIBLE);
		String description = etDescription.getText().toString();

		etDescription.setVisibility(View.INVISIBLE);
		tvDescription.setVisibility(View.VISIBLE);
		tvDescription.setText(description);
		tagsav.setDescription(description);
		setPrecision();
		dm = new DatabaseManager(getBaseContext());
		dm.saveTagSav(tagsav);
		dm.close();
		Toast.makeText(getBaseContext(), "Saved Successfully!!",
				Toast.LENGTH_SHORT).show();
		

	}

}
