/*
 * Kanaparthi Tarun Santosh
 */
package com.tkanapar.tagfav;

import java.io.File;
import java.util.ArrayList;

import com.tkanapar.adapter.ImageAdapter;

import android.app.Activity;
import android.os.Bundle;
import android.widget.GridView;
import android.widget.Toast;

public class GalleryActivity extends Activity {
	static ArrayList<String> filePath = new ArrayList<String>();
	File[] fileList;
	String folderPath;
	GridView gridView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.gallery_main);
		folderPath = getIntent().getExtras().getString(
				DisplaySavedActivity.GALLERY_KEY);
		filePath.clear();
		 getFilesFromFolder();

		if (fileList !=null) {
			gridView = (GridView) findViewById(R.id.PhoneImageGrid);
			gridView.setAdapter(new ImageAdapter(this, fileList));
		} else {
			Toast.makeText(this, "The file path is empty!!", Toast.LENGTH_SHORT)
					.show();
		}
	}

	private void getFilesFromFolder() {
		File file = new File(folderPath);

		if (file.isDirectory()) {
			if (file.list().length > 0) {
				fileList = file.listFiles();

				for (int i = 0; i < fileList.length; i++) {

					filePath.add(fileList[i].getAbsolutePath());

				}
				

			}else{
				Toast.makeText(this, "The folder path is empty!!",
						Toast.LENGTH_SHORT).show();
			}
			
		}
	}

}
