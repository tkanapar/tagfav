/*
 * Kanaparthi Tarun Santosh
 */
package com.tkanapar.tagfav;

import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.tkanapar.fragment.TagFragment;
import com.tkanapar.sqldb.DatabaseManager;
import com.tkanapar.util.StringUtil;
import com.tkanapar.util.TagSav;
import com.tkanapar.util.TagSavUtil;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Bitmap;
import android.location.Address;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.NavUtils;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class DisplaySavedActivity extends ActionBarActivity implements
		LocationListener {
	Address address;
	ImageView imSnapMapd;
	TextView tvAddressd;
	TextView tvDated;
	TextView tvDescriptiond;
	EditText etDescription;
	TagSav tagsav;
	Button bSave;
	Button bBack;
	DatabaseManager dm;
	Double currLongitude;
	Double currLatitude;
	String basePath;
	String folderPath;
	static int RESULT_CHECK = 10;
	static final int REQUEST_TAKE_PHOTO = 1;
	static final String GALLERY_KEY = "GALLERYKEY";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.display_view);
		RESULT_CHECK = 10;
		imSnapMapd = (ImageView) findViewById(R.id.imSnapMapd);
		tvAddressd = (TextView) findViewById(R.id.tvAddressd);
		tvDescriptiond = (TextView) findViewById(R.id.tvDescriptiond);
		tvDated = (TextView) findViewById(R.id.tvDated);
		etDescription = (EditText) findViewById(R.id.etDescription);
		bSave = (Button) findViewById(R.id.bdsave);
		bBack = (Button) findViewById(R.id.bdback);
		MainActivity.locationManager.requestLocationUpdates(
				LocationManager.NETWORK_PROVIDER, TagFragment.MIN_TIME,
				TagFragment.MIN_DISTANCE, this);
		resetVisibility();
		setUpLayout();
		setListener();
	}

	@Override
	public void finish() {
		Intent data = new Intent();
		data.putExtra("tagsavup", tagsav);
		setResult(RESULT_CHECK,data);
		super.finish();
	}

	private void setListener() {

		imSnapMapd.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Toast.makeText(
						getBaseContext(),
						"Long press to navigate to "
								+ tagsav.getAddress().getAddressLine(0),
						Toast.LENGTH_SHORT).show();
			}
		});

		imSnapMapd.setOnLongClickListener(new OnLongClickListener() {
			@Override
			public boolean onLongClick(View v) {
				Double latitude = tagsav.getAddress().getLatitude();
				Double longitude = tagsav.getAddress().getLongitude();
				if (latitude == currLatitude && longitude == currLongitude) {
					Toast.makeText(getBaseContext(), "We are already here!!",
							Toast.LENGTH_SHORT).show();
					return false;
				}
				if (latitude == null || longitude == null) {
					Toast.makeText(getBaseContext(),
							"Unable to get present location!!",
							Toast.LENGTH_SHORT).show();
					return false;
				}
				String uri = "http://maps.google.com/maps?saddr="
						+ currLatitude + "," + currLongitude + "&daddr="
						+ latitude + "," + longitude;
				Intent intent = new Intent(android.content.Intent.ACTION_VIEW,
						Uri.parse(uri));
				intent.setClassName("com.google.android.apps.maps",
						"com.google.android.maps.MapsActivity");

				startActivity(intent);
				return true;
			}
		});

	}

	void resetVisibility() {
		bSave.setVisibility(View.INVISIBLE);
		bBack.setVisibility(View.VISIBLE);
		etDescription.setVisibility(View.INVISIBLE);
		tvDescriptiond.setVisibility(View.VISIBLE);

	}

	void toggleVisibility() {
		bSave.setVisibility(View.VISIBLE);
		bBack.setVisibility(View.INVISIBLE);
		etDescription.setVisibility(View.VISIBLE);
		tvDescriptiond.setVisibility(View.INVISIBLE);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.display_savedlist_items, menu);
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			NavUtils.navigateUpFromSameTask(this);
			return true;
		case R.id.action_edit:
			EditTextAction();
			return true;
		case R.id.action_camera:
			CameraAction();
			return true;
		case R.id.action_gallery:
			GalleryAction();
			return true;
		}
		return false;
	}

	private void GalleryAction() {
		Intent intent = new Intent(DisplaySavedActivity.this, GalleryActivity.class);
		intent.putExtra(GALLERY_KEY, folderPath);
		startActivity(intent);
	}

	@SuppressLint("SimpleDateFormat")
	private void CameraAction() {
		DateFormat df = new SimpleDateFormat("dd-MM-yy_HH:mm:ss");
		String fileName = tagsav.get_ID()+"_"+df.format(new Date())+".jpg";
		folderPath = basePath+"/"+tagsav.get_ID()+"/";
		File image = new File(folderPath,fileName);
	    Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
	    if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
	    	if (image != null) {
	            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
	                    Uri.fromFile(image));
	            startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);
	        }
	    }
		
	}

	private void EditTextAction() {
		RESULT_CHECK = 20;
		toggleVisibility();
		etDescription.setText(tagsav.getDescription());

	}

	public void Save(View view) {
		String descriptionUpdate;
		descriptionUpdate = etDescription.getText().toString();
		resetVisibility();
		dm = new DatabaseManager(this);
		tagsav.setDescription(descriptionUpdate);
		dm.updateTagSav(tagsav);
		dm.close();
		Toast.makeText(this, "Updated the description", Toast.LENGTH_SHORT)
				.show();
		tvDescriptiond.setText(descriptionUpdate);

	}

	public void setUpLayout() {
		tagsav = (TagSav) getIntent().getExtras().getParcelable(
				StringUtil.DTAGSAV_KEY);
		basePath = getExternalFilesDir(null) +"/";
		folderPath = basePath+"/"+tagsav.get_ID()+"/";
		address = tagsav.getAddress();
		String formattedDate = tagsav.getFormattedDate();

		String filename = StringUtil.SNAPFILE + formattedDate + ".jpg";

		Bitmap bitmap = TagSavUtil.decodeSampledBitmapFromResource(
				basePath + filename, 480, 220);

		if (bitmap != null) {
			imSnapMapd.setImageBitmap(bitmap);
		}
		tvDated.setText(formattedDate.replace("_", " "));
		if (address != null) {
			Double latitude = TagSavUtil
					.roundFiveDecimal(address.getLatitude());
			Double longitude = TagSavUtil.roundFiveDecimal(address
					.getLongitude());

			StringBuilder displayAddress = new StringBuilder();
			displayAddress.append(address.getAddressLine(0));
			displayAddress.append("\n");

			displayAddress.append(address.getAddressLine(1) + " "
					+ address.getAddressLine(2));
			displayAddress.append("\n");
			displayAddress.append("Latitude=" + latitude);
			displayAddress.append(" Longitude=" + longitude);
			tvAddressd.setText(displayAddress.toString());
		}
		tvDescriptiond.setText(tagsav.getDescription());
	}

	public void Back(View view) {
		finish();
	}

	@Override
	public void onLocationChanged(Location location) {
		currLatitude = TagSavUtil.roundFiveDecimal(location.getLatitude());
		currLongitude = TagSavUtil.roundFiveDecimal(location.getLongitude());

	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {

	}

	@Override
	public void onProviderEnabled(String provider) {

	}

	@Override
	public void onProviderDisabled(String provider) {

	}
}
