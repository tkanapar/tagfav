/*
 * Kanaparthi Tarun Santosh
 */
package com.tkanapar.tagfav;

import com.tkanapar.adapter.OnSwipeTouchListener;
import com.tkanapar.util.TagSavUtil;

import android.app.Activity;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

public class FullImageActivity extends Activity {
	ImageView imageView;
	static int position;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.full_image);
        position = getIntent().getExtras().getInt("IMAGEADAPTERKEY");
        imageView = (ImageView) findViewById(R.id.full_image_view);
        Bitmap bitmap = TagSavUtil.decodeSampledBitmapFromResource(
				GalleryActivity.filePath.get(position), 500, 500); 
        imageView.setImageBitmap(bitmap);
        imageView.setOnTouchListener(new OnSwipeTouchListener(this){

			@Override
			public void onSwipeRight() {
				super.onSwipeRight();
			 nextAction(imageView);
			}

			@Override
			public void onSwipeLeft() {
				super.onSwipeLeft();
		   previousAction(imageView);
			}
			
		});
        
    }
    public void loadImage(){
    	 Bitmap bitmap = TagSavUtil.decodeSampledBitmapFromResource(
 				GalleryActivity.filePath.get(position), 500, 500); 
         imageView.setImageBitmap(bitmap);

    }
    public void previousAction(View view) {
		if (position > 0) {
			position--;
		} else {
			position = GalleryActivity.filePath.size() - 1;

		}
		loadImage();
	}

	public void nextAction(View view) {
		if (position < GalleryActivity.filePath.size() - 1) {
			position++;
		} else {
			position = 0;
		}
		loadImage();
	}
 
}