/*
 * Kanaparthi Tarun Santosh
 */
package com.tkanapar.fragment;

import java.io.File;
import java.util.ArrayList;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.Toast;

import com.tkanapar.adapter.TagSavDisplayAdapter;
import com.tkanapar.sqldb.DatabaseManager;
import com.tkanapar.tagfav.DisplaySavedActivity;
import com.tkanapar.tagfav.MainActivity;
import com.tkanapar.tagfav.R;
import com.tkanapar.util.StringUtil;
import com.tkanapar.util.TagSav;

public class FavFragment extends Fragment implements OnItemClickListener {
	View rootView = null;
	TagSavDisplayAdapter tagSavDisplayAdapter;
	DatabaseManager dm;
	ListView listView;
	ArrayList<TagSav> tagsavs = null;
	static final int REQUEST_IMAGE_CAPTURE = 1;
	static final int REQUEST_CHANGE_TEXT = 199;
	Bitmap resultBitmap;
	String filename;
	File file;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		if (rootView != null) {
			((ViewGroup) rootView.getParent()).removeAllViews();

		}
		if (rootView == null) {
			rootView = inflater
					.inflate(R.layout.fragment_fav, container, false);
			listView = (ListView) rootView.findViewById(R.id.lvFav);
		}
		setupFavDisplay();
		return rootView;
	}

	public void setupFavDisplay() {

		dm = new DatabaseManager(MainActivity.context);
		if (tagsavs != null)
			tagsavs.clear();
		tagsavs = dm.getAllTagSavs();
		dm.close();

		if (tagsavs == null) {
			Toast.makeText(MainActivity.context, "No tags saved !!",
					Toast.LENGTH_LONG).show();
			return;
		}
		tagSavDisplayAdapter = new TagSavDisplayAdapter(getActivity(), tagsavs);
		if (tagSavDisplayAdapter == null) {
			Toast.makeText(MainActivity.context, "Problem with adapter !!",
					Toast.LENGTH_LONG).show();
			return;
		}
		listView.setAdapter(tagSavDisplayAdapter);
		listView.setOnItemClickListener(this);
		registerForContextMenu(listView);
	}

	@Override
	public boolean onContextItemSelected(MenuItem item) {
		AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item
				.getMenuInfo();
		int menuItemIndex = item.getItemId();
		String[] menuItems = getResources().getStringArray(R.array.menu);
		String menuItemName = menuItems[menuItemIndex];
		TagSav tagsav = tagsavs.get(info.position);

		Integer _ID = tagsav.get_ID();
		final String formattedDate = tagsav.getFormattedDate();
		filename = StringUtil.SNAPFILE + formattedDate + ".jpg";
		String filePath = MainActivity.context.getExternalFilesDir(null) + "/"
				+ filename;
		file = new File(filePath);
		if (menuItemName.equals("Delete")) {
			tagSavDisplayAdapter.remove(tagsav);
			tagSavDisplayAdapter.notifyDataSetChanged();
			file.delete();
			String basePath = MainActivity.context.getExternalFilesDir(null) +"/";
			String folderPath = basePath+"/"+_ID;
			String changePath = basePath+"/old"+_ID+formattedDate;
			File tmpFolder = new File(folderPath);
			tmpFolder.renameTo(new File(changePath));

			dm = new DatabaseManager(MainActivity.context);
			dm.deleteTagSav(_ID);
			dm.close();
			Toast.makeText(MainActivity.context, "Deleted " + _ID,
					Toast.LENGTH_SHORT).show();
		} else if (menuItemName.equals("Change Picture")) {
			tagSavDisplayAdapter.remove(tagsav);
			CameraAction();
			//tagSavDisplayAdapter.insert(tagsav, info.position);

		}

		return true;
	}

	@SuppressLint("SimpleDateFormat")
	private void CameraAction() {
		file = new File(MainActivity.context.getExternalFilesDir(null),
				filename);
		Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		if (takePictureIntent.resolveActivity(MainActivity.context
				.getPackageManager()) != null) {
			if (file != null) {
				takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
						Uri.fromFile(file));
				startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
			}
		}
	}

	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == REQUEST_IMAGE_CAPTURE
				&& resultCode == MainActivity.RESULT_OK) {
			tagSavDisplayAdapter.notifyDataSetChanged();

		}
	}

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v,
			ContextMenuInfo menuInfo) {
		if (v.getId() == R.id.lvFav) {
			AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuInfo;
			menu.setHeaderTitle(tagsavs.get(info.position).getFormattedDate()
					.replace("_", " "));
			String[] menuItems = getResources().getStringArray(R.array.menu);
			for (int i = 0; i < menuItems.length; i++) {
				menu.add(Menu.NONE, i, i, menuItems[i]);
			}
		}
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		TagSav tagsav = tagsavs.get(position);
		Intent intent = new Intent(getActivity(), DisplaySavedActivity.class);
		intent.putExtra(StringUtil.DTAGSAV_KEY, tagsav);
		startActivityForResult(intent, REQUEST_CHANGE_TEXT);
	}

}
