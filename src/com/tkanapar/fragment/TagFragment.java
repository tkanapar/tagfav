/*
 * Kanaparthi Tarun Santosh
 */
package com.tkanapar.fragment;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnInfoWindowClickListener;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.tkanapar.async.SnapShotAsync;
import com.tkanapar.tagfav.MainActivity;
import com.tkanapar.tagfav.R;
import com.tkanapar.tagfav.TagFavSavActivity;
import com.tkanapar.util.StringUtil;
import com.tkanapar.util.TagSav;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Bitmap;
import android.location.Address;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

@SuppressLint("SimpleDateFormat")
public class TagFragment extends Fragment implements
		GoogleMap.OnMapClickListener, GoogleMap.OnMapLongClickListener,
		LocationListener, OnInfoWindowClickListener {
	public GoogleMap gMap;
	Address address;
	public String formattedDate;
	public Bitmap snapMap;
	public String filename;
	public static final long MIN_TIME = 400;
	public static final float MIN_DISTANCE = 1000;
	View rootView = null;
	Integer tagsavInt=9519;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		if (rootView != null) {
			((ViewGroup) rootView.getParent()).removeAllViews();

		}
		if (rootView == null) {
			rootView = inflater
					.inflate(R.layout.fragment_tag, container, false);
		}

		setTheTagMap();
		return rootView;
	}

	public void setTheTagMap() {

		if (gMap == null) {
			gMap = ((MapFragment) MainActivity.fragmentManager
					.findFragmentById(R.id.tagMap)).getMap();
		}
		if (gMap != null) {
			gMap.setMyLocationEnabled(true);
			MainActivity.locationManager.requestLocationUpdates(
					LocationManager.NETWORK_PROVIDER, MIN_TIME, MIN_DISTANCE,
					this);
			gMap.setOnMapClickListener(this);
			gMap.setOnMapLongClickListener(this);
			gMap.setOnInfoWindowClickListener(this);
		}
	}

	@Override
	public void onMapClick(LatLng position) {
		try {
			address = MainActivity.geoCoder.getFromLocation(position.latitude,
					position.longitude, 1).get(0);
			Toast.makeText(
					MainActivity.context,
					"Currently at " + address.getAddressLine(0)
							+ " long press to mark", Toast.LENGTH_SHORT).show();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onMapLongClick(LatLng position) {
		try {
			MainActivity.v.vibrate(60);
			address = MainActivity.geoCoder.getFromLocation(position.latitude,
					position.longitude, 1).get(0);
			MarkerOptions markerOptions = new MarkerOptions()
					.position(position).snippet(address.getAddressLine(1))
					.title(address.getAddressLine(0));
			gMap.addMarker(markerOptions);
			DateFormat df = new SimpleDateFormat("dd-MM-yy_HH:mm:ss");
			formattedDate = df.format(new Date());
			filename = StringUtil.SNAPFILE + formattedDate+".jpg";
			new SnapShotAsync(this).execute(filename);
			Toast.makeText(MainActivity.context, "Snapshot taken !!",
					Toast.LENGTH_SHORT).show();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onLocationChanged(Location location) {
		LatLng latLng = new LatLng(location.getLatitude(),
				location.getLongitude());
		gMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 18));
	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {

	}

	@Override
	public void onProviderEnabled(String provider) {

	}

	@Override
	public void onProviderDisabled(String provider) {

	}

	@Override
	public void onInfoWindowClick(Marker arg0) {
		// TODO Auto-generated method stub
		Intent intent = new Intent(MainActivity.context,
				TagFavSavActivity.class);
		TagSav tagsav = new TagSav();
		tagsav.setAddress(address);
		tagsav.setFormattedDate(formattedDate);
		tagsav.set_ID(tagsavInt);
		intent.putExtra(StringUtil.TAGSAV_KEY, tagsav);
		startActivityForResult(intent,0);

	}
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		gMap.clear();
		//super.onActivityResult(requestCode, resultCode, data);
	}

}
