/*
 * Kanaparthi Tarun Santosh
 */
package com.tkanapar.fragment;

import java.io.IOException;
import java.util.ArrayList;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.tkanapar.adapter.CustomInfoAdapter;
import com.tkanapar.sqldb.DatabaseManager;
import com.tkanapar.tagfav.DisplaySavedActivity;
import com.tkanapar.tagfav.DisplayTagsActivity;
import com.tkanapar.tagfav.MainActivity;
import com.tkanapar.tagfav.R;
import com.tkanapar.util.StringUtil;
import com.tkanapar.util.TagSav;
import com.tkanapar.util.TagSavUtil;

import android.content.Intent;
import android.location.Address;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

public class ViewFragment extends Fragment implements LocationListener,
		GoogleMap.OnInfoWindowClickListener,GoogleMap.OnMapClickListener {
	View rootView = null;
	GoogleMap gMap;
	private static final long MIN_TIME = 400;
	private static final float MIN_DISTANCE = 1000;
	ArrayList<TagSav> tagsavs = null;
	DatabaseManager dm;
	ListView listView;
	LayoutInflater inflater;
	ViewGroup container;
	static int RESULT_SINGLE = 1;
	static int RESULT_MULTIPLE = 2;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		this.inflater = inflater;
		this.container = container;
		if (rootView != null) {
			((ViewGroup) rootView.getParent()).removeAllViews();

		}
		if (rootView == null) {
			rootView = inflater.inflate(R.layout.fragment_view, container,
					false);

		}

		viewMapSetUp();
		return rootView;
	}

	public void viewMapSetUp() {
		if (gMap == null) {
			gMap = ((MapFragment) MainActivity.fragmentManager
					.findFragmentById(R.id.viewMap)).getMap();
		}
		if (gMap != null) {
			gMap.clear();
			gMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
			gMap.setMyLocationEnabled(true);
			gMap.setOnInfoWindowClickListener(this);
			gMap.setInfoWindowAdapter(new CustomInfoAdapter(this));
			gMap.setOnMapClickListener(this);
			
			MainActivity.locationManager.requestLocationUpdates(
					LocationManager.NETWORK_PROVIDER, MIN_TIME, MIN_DISTANCE,
					this);
			dm = new DatabaseManager(MainActivity.context);
			if (tagsavs != null)
				tagsavs.clear();
			tagsavs = dm.getAllTagSavsAsc();
			dm.close();
			if (tagsavs != null) {
				LatLng latlng;
				MarkerOptions markeroptions;
				for (TagSav tagsav : tagsavs) {
					latlng = new LatLng(tagsav.getAddress().getLatitude(),
							tagsav.getAddress().getLongitude());
					
					String snippet = tagsav.getAddress().getAddressLine(0)
							+ "|" + tagsav.getDescription();
					markeroptions = new MarkerOptions();
					markeroptions.position(latlng);
					markeroptions.snippet(snippet);
					markeroptions.title(tagsav.getFormattedDate());
					gMap.addMarker(markeroptions);
				}
			}

		}
	}
	@Override
	public void onMapClick(LatLng position) {
		try {
			Address address = MainActivity.geoCoder.getFromLocation(position.latitude,
					position.longitude, 1).get(0);
			Toast.makeText(
					MainActivity.context,
					 address.getAddressLine(0)
							+ " "+ address.getAddressLine(1), Toast.LENGTH_SHORT).show();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	@Override
	public void onLocationChanged(Location location) {
		LatLng latLng = new LatLng(location.getLatitude(),
				location.getLongitude());
		gMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15));
	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onProviderEnabled(String provider) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onProviderDisabled(String provider) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {

		if (requestCode == RESULT_MULTIPLE || requestCode == RESULT_SINGLE) {
			viewMapSetUp();

		}
		super.onActivityResult(requestCode, resultCode, data);
	}

	@Override 
	public void onInfoWindowClick(Marker marker) {

		Double latitude = TagSavUtil
				.roundFiveDecimal(marker.getPosition().latitude);
		Double longitude = TagSavUtil
				.roundFiveDecimal(marker.getPosition().longitude);
		LatLng latlng = new LatLng(latitude, longitude);
		dm = new DatabaseManager(MainActivity.context);
		ArrayList<TagSav> list = dm.getTagFromLatLng(latlng);
		dm.close();
		if (list.size() == 1) {
			TagSav tagsav = list.get(0);
			Intent intent = new Intent(MainActivity.context,
					DisplaySavedActivity.class);
			intent.putExtra(StringUtil.DTAGSAV_KEY, tagsav);
			startActivityForResult(intent, RESULT_SINGLE);
		}
		if (list.size() > 1) {
			Intent intent = new Intent(MainActivity.context,
					DisplayTagsActivity.class);
			intent.putParcelableArrayListExtra(StringUtil.TAGSAVS_KEY, list);
			startActivityForResult(intent, RESULT_MULTIPLE);

		}

	}

}
