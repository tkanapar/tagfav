/*
 * Kanaparthi Tarun Santosh
 */
package com.tkanapar.adapter;

import android.graphics.Bitmap;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.maps.GoogleMap.InfoWindowAdapter;
import com.google.android.gms.maps.model.Marker;
import com.tkanapar.fragment.ViewFragment;
import com.tkanapar.tagfav.MainActivity;
import com.tkanapar.tagfav.R;
import com.tkanapar.util.StringUtil;
import com.tkanapar.util.TagSavUtil;

public class CustomInfoAdapter implements InfoWindowAdapter {
	ViewFragment viewfragment;

	public CustomInfoAdapter(ViewFragment viewfragment) {
		this.viewfragment = viewfragment;

	}

	@Override
	public View getInfoContents(Marker marker) {
		View myContentView = viewfragment.getActivity().getLayoutInflater()
				.inflate(R.layout.view_custominfo, null);
		TextView header = (TextView) myContentView.findViewById(R.id.header);
		String formattedDate = marker.getTitle();
		header.setText(formattedDate.replace("_", " "));

		String filename = StringUtil.SNAPFILE + formattedDate + ".jpg";
		String snippet[] = marker.getSnippet().split("\\|");
		Bitmap bitmap = TagSavUtil
				.decodeSampledBitmapFromResource(
						MainActivity.context.getExternalFilesDir(null) + "/"
								+ filename, 480, 220);
		TextView description = (TextView) myContentView
				.findViewById(R.id.description);
		TextView area = (TextView) myContentView.findViewById(R.id.area);
		ImageView iv = (ImageView) myContentView.findViewById(R.id.imsnapImage);
		iv.setImageBitmap(bitmap);
		description.setText(snippet[1].toString());
		area.setText(snippet[0].toString());

		return myContentView;
	}

	@Override
	public View getInfoWindow(Marker arg0) {
		return null;
	}

}
