/*
 * Kanaparthi Tarun Santosh
 */
package com.tkanapar.adapter;

import java.io.File;

import com.tkanapar.tagfav.FullImageActivity;
import com.tkanapar.tagfav.R;
import com.tkanapar.util.TagSavUtil;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

public class ImageAdapter extends BaseAdapter {
	private Context mContext;
	private Activity activity;
	 File[] fileList;

	static class RecordHolder {
		ImageView ivThumbView;
	}

	public ImageAdapter(Activity a, File[] fl) {
		activity = a;
		mContext = a.getBaseContext();
		fileList = fl;
	}

	@Override
	public int getCount() {
		return fileList.length;
	}

	@Override
	public Object getItem(int position) {
		return fileList[position];
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		RecordHolder holder;
		if (convertView == null) {
			holder = new RecordHolder();
			LayoutInflater li = LayoutInflater.from(mContext);
			convertView = li.inflate(R.layout.gallery_item, null);
			holder.ivThumbView = (ImageView) convertView
					.findViewById(R.id.ivThumbView);
			convertView.setTag(holder);
		} else {
			holder = (RecordHolder) convertView.getTag();
		}
		Bitmap bitmap = TagSavUtil.decodeSampledBitmapFromResource(
				fileList[position].toString(), 100, 100);
		holder.ivThumbView.setImageBitmap(bitmap);
		holder.ivThumbView.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(activity, FullImageActivity.class);
				intent.putExtra("IMAGEADAPTERKEY",
						position);
				activity.startActivity(intent);
			}
		});
		return convertView;
	}

}