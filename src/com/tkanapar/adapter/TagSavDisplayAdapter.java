/*
 * Kanaparthi Tarun Santosh
 */
package com.tkanapar.adapter;

import java.util.ArrayList;
import java.util.List;

import com.tkanapar.tagfav.R;
import com.tkanapar.util.StringUtil;
import com.tkanapar.util.TagSav;
import com.tkanapar.util.TagSavUtil;

import android.app.Activity;
import android.graphics.Bitmap;
import android.location.Address;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class TagSavDisplayAdapter extends ArrayAdapter<TagSav> {
	Activity activity;
	List<TagSav> tagsavs = new ArrayList<TagSav>();
	
	 public TagSavDisplayAdapter(Activity activity, List<TagSav> objects) {
	        super(activity, R.layout.custom_listview , objects);
	        this.activity = activity;
	        this.tagsavs = objects;
	    }

	static class RecordHolder {
		ImageView imsnapshotimage;
		TextView tvheadingV;
		TextView tvAddressV;
		TextView tvDescriptionV;
		ImageView imShare;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View row = convertView;
		RecordHolder holder = null;
		if (row == null) {
			LayoutInflater inflater = activity.getLayoutInflater();
            row = inflater.inflate(R.layout.custom_listview, null);
            
			holder = new RecordHolder();
			holder.imsnapshotimage = (ImageView) row
					.findViewById(R.id.imsnapshotimage);
			holder.tvheadingV = (TextView) row.findViewById(R.id.tvheadingV);
			holder.tvAddressV = (TextView) row.findViewById(R.id.tvAddressV);
			holder.tvDescriptionV = (TextView) row
					.findViewById(R.id.tvDescriptionV);
			holder.imShare = (ImageView) row.findViewById(R.id.imShare);
			row.setTag(holder);
		} else {
			holder = (RecordHolder) row.getTag();
		}

		final TagSav tagsav = tagsavs.get(position);
		final String formattedDate = tagsav.getFormattedDate();
		Address address = tagsav.getAddress();

		String filename = StringUtil.SNAPFILE + formattedDate + ".jpg";
		Bitmap bitmap = TagSavUtil.decodeSampledBitmapFromResource(
				activity.getExternalFilesDir(null) + "/" + filename, 100, 100);

		holder.imsnapshotimage.setImageBitmap(bitmap);
		holder.tvheadingV.setText(formattedDate.replace("_", " "));

		if (address != null) {
			StringBuilder displayAddress = new StringBuilder();
			displayAddress.append(address.getAddressLine(0));
			displayAddress.append("\n");
			displayAddress.append(address.getAddressLine(1) + " "+ address.getAddressLine(2));
			displayAddress.append("\n");
			displayAddress.append("Latitude=" + address.getLatitude());
			displayAddress.append("Longitude=" + address.getLongitude());
			holder.tvAddressV.setText(displayAddress.toString());
		}
		holder.tvDescriptionV.setText(tagsav.getDescription());
		holder.imShare.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Toast.makeText(activity, "Event "+formattedDate+" share to facebook!!", Toast.LENGTH_SHORT).show();
			}
		});
		return row;
	}
}
