/*
 * Kanaparthi Tarun Santosh
 */
package com.tkanapar.util;

import java.io.File;
import java.text.DecimalFormat;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.text.Html;
import android.text.Spanned;

public abstract class TagSavUtil {

	public static boolean buildFilePath(String filepath){
		File file = new File(filepath);
		return file.mkdirs();
	}
	
	public static Double roundFiveDecimal(Double d) {
		DecimalFormat twoDForm = new DecimalFormat("#.#####");
		return Double.valueOf(twoDForm.format(d));

	}

	public static String markedUpString(String htmlText) {
		Spanned marked_up = Html.fromHtml(htmlText);
		return marked_up.toString();
	}

	public static Bitmap decodeSampledBitmapFromResource(String pathName,
			int reqWidth, int reqHeight) {

		final BitmapFactory.Options options = new BitmapFactory.Options();
		options.inJustDecodeBounds = true;
		BitmapFactory.decodeFile(pathName, options);

		options.inSampleSize = calculateInSampleSize(options, reqWidth,
				reqHeight);
		options.inJustDecodeBounds = false;
		return BitmapFactory.decodeFile(pathName, options);
	}
	

	public static int calculateInSampleSize(BitmapFactory.Options options,
			int reqWidth, int reqHeight) {
		final int height = options.outHeight;
		final int width = options.outWidth;
		int inSampleSize = 1;

		if (height > reqHeight || width > reqWidth) {

			final int halfHeight = height / 2;
			final int halfWidth = width / 2;

			while ((halfHeight / inSampleSize) > reqHeight
					&& (halfWidth / inSampleSize) > reqWidth) {
				inSampleSize *= 2;
			}
		}

		return inSampleSize;
	}
}
