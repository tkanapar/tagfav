/*
 * Kanaparthi Tarun Santosh
 */
package com.tkanapar.util;

import android.location.Address;
import android.os.Parcel;
import android.os.Parcelable;

public class TagSav implements Parcelable {
	private Integer _ID;
	private String formattedDate;
	private Address address;
	private String description;
	

	public Integer get_ID() {
		return _ID;
	}

	public void set_ID(Integer _ID) {
		this._ID = _ID;
	}

	public String getFormattedDate() {
		return formattedDate;
	}

	public void setFormattedDate(String formattedDate) {
		this.formattedDate = formattedDate;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(formattedDate);
		dest.writeParcelable(address, flags);
		dest.writeString(description);
		dest.writeInt(_ID);
	}

	public static final Parcelable.Creator<TagSav> CREATOR = new Parcelable.Creator<TagSav>() {
		public TagSav createFromParcel(Parcel source) {
			TagSav tagsav = new TagSav(source);
			return tagsav;
		}

		public TagSav[] newArray(int size) {
			return new TagSav[size];
		}
	};

	private TagSav(Parcel source) {
		formattedDate = source.readString();
		address = source.readParcelable(Address.class.getClassLoader());
		description = source.readString();
		_ID = source.readInt();
	}

	public TagSav() {
	}
}
