/*
 * Kanaparthi Tarun Santosh
 */
package com.tkanapar.util;

public interface StringUtil {
 String ADDRESS_KEY="address";
 String FILENAME_KEY ="snapshotfile";
 String SNAPFILE = "snapshot";
 String CAMFILE = "camera";
 String TIME_KEY ="currenttime";
 String TAGSAV_KEY = "tagsavparcelable";
 String DTAGSAV_KEY = "displaytagsavparcelable";
 String TAGSAVS_KEY = "sameplacemorecheckin";

}
